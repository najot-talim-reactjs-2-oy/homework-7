import { createContext, useState } from "react";
import PropTypes from 'prop-types';
import { products } from "../data/products";
import { toast } from "react-toastify";

export const ProductContext = createContext();

// const initialState = {
//   cartProducts: []
// };

// const reducer = (state, {type, payload}) => {
//   const { cartProducts } = state;
//   switch(type){
//     case "add-to-cart": {
//       const product = products.find(pr => pr.id === payload);
//       const check = cartProducts.find((pr) => pr.id === payload);
//       if(check){
//         toast.success("Increase product quantity !");
//         let newCartProdudcts = cartProducts.map((pr) => {
//           pr.id === payload && pr.quantity++;
//           return pr;
//         });
//         return {
//           ...state,
//           cartProducts: newCartProdudcts,
//         };
//       }else{
//         toast.success("Add to cart !");
//         return {
//           ...state,
//           cartProducts: [...state.cartProducts, {...product, quantity: 1}],
//         };
//       }
//     }
//       default:
//         return state
//   }
// };


const ProductContextProvide = ({ children }) => {
  // const [state, dispatch] = useReducer(reducer, initialState);

  // const contextState = { ...state, dispatch };

  const [cartProducts, setCartProducts] = useState(
    JSON.parse(localStorage.getItem("cart")) || []
  );

  const addToCart = (id) => {
    const product = products.find((pr) => pr.id === id);
    const check = cartProducts.find((pr) => pr.id === id);

    let newCartProducts;

    if (check) {
      toast.success("Increase product quantity !");
      newCartProducts = cartProducts.map((pr) => {
        pr.id === id && pr.quantity++;
        return pr;
      });
    } else {
      toast.success("Add to cart !");
      newCartProducts = [...cartProducts, { ...product, quantity: 1 }];
    }

    setCartProducts(newCartProducts);
    localStorage.setItem("cart", JSON.stringify(newCartProducts));
  };
  
  const increaseQuantity = (id) => {
    let newCartProducts = cartProducts.map((pr) => {
        pr.id === id && pr.quantity++;
        return pr;
    });
    setCartProducts(newCartProducts);
    localStorage.setItem("cart", JSON.stringify(newCartProducts));
  }
  const decreaseQuantity = (id) => {
    const cartProduct = cartProducts.find((pr) => pr.id === id);
    let newCartProducts = [...cartProducts]; 
    if(cartProduct.quantity > 1){
      newCartProducts = cartProducts.map((pr) => {
          pr.id === id && pr.quantity--;
          return pr;
      });
    }else{
      let deleteConfirm = confirm("Are you sure you want to delete ?")
      if(deleteConfirm) {
        newCartProducts = cartProducts.filter((pr) => pr.id != id)
      }
    }
    setCartProducts(newCartProducts);
    localStorage.setItem("cart", JSON.stringify(newCartProducts));
  }

  const contextState = {
    cartProducts,
    addToCart,
    increaseQuantity,
    decreaseQuantity
  };

  return (
    <ProductContext.Provider value={contextState}>
      {children}
    </ProductContext.Provider>
  );
};

ProductContextProvide.propTypes = {
    children: PropTypes.node,
}


export default ProductContextProvide