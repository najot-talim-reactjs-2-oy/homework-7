import group from '../../assets/Group61.png'
import telephone from '../../assets/telephone.png'
import Location from '../../assets/Location.png'
import './footer.css'

const Footer = () => {
  return (
    <footer>
      <div className='flex-footer container'>
        <div>
          <img width="140px" src={group} alt="" />
        </div>

        <div>
          <p className='kuda'>Куда Пицца</p>
          <div className='text-footer'>
            <a href='/'>О компании</a><br />
            <a href='/'>Пользовательское соглашение</a><br />
            <a href='/'>Условия гарантии</a>
          </div>
        </div>
        <div>
          <p className='kuda'>Помощь</p> 
          <div className='text-footer'>
            <a href='/'>Ресторан</a><br />
            <a href='/'>Контакты</a><br />
            <a href='/'>Поддержка</a><br />
            <a href='/'>Отследить заказ</a>
          </div>
        </div>
        <div>
          <p className='kuda'>Контакты</p>
          <div className='kontact-fle-tel'>
            <img src={telephone} alt="" />
            <p>+7 (926) 223-10-11</p>
          </div>
          <div className='kontact-fle-tel'>
            <img src={Location} alt="" />
            <p> Москва, ул. Юных Ленинцев, д.99</p>
          </div>
        </div>
      </div>
      <div className='container' style={{padding: "0px 10px"}}>
        <p>© Copyright 2021 — Куда Пицца</p><br />
      </div>
    </footer>
  )
}

export default Footer