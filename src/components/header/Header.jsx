import Location from "../../assets/Location.png";
import Account from "../../assets/Account.png";
import navLogo from "../../assets/navLogo.png";
import Shopping from "../../assets/Shopping bag.png";
import { useContext } from "react";
import './header.css'
import { ProductContext } from "../../context/ProductContext";
import { NavLink } from "react-router-dom";




const Header = () => {
  // let time = new Date().toLocaleTimeString();
  // const [currentTime, setCurrentTime] = useState(time)

  // const updateTime = () => {
  //   let time = new Date().toLocaleTimeString();
  //   setCurrentTime(time)
  // }
  // setInterval(updateTime, 1000)

  const {cartProducts} = useContext(ProductContext);
  
  return (
    <div className="positon">
      <header>
        <div className="flex container">
          <div className="left-fl">
            <div className="left_location">
              <img src={Location} alt="" />
              <select name="cars" id="cars">
                <option value="volvo">Москва</option>
                <option value="saab">Америка</option>
                <option value="mercedes">Кина</option>
                <option value="audi">Египет</option>
              </select>
            </div>
            <div>
              <a href="https://www.google.com/maps" target="_blank_">Проверить адрес</a>
            </div>
            <div className="left-time">
              {/* <h4>{currentTime}</h4> <div className="clock2"></div> */}
              <a href="https://www.google.com/maps" target="_blank_">Среднее время доставки*:00:24:19 </a>
            </div>
          </div>

          <div className="right">
            <div className="left-time">
              <a href="https://www.google.com/maps/place/%D0%A2%D0%B0%D1%88%D0%BA%D0%B5%D0%BD%D1%82,+%D0%A3%D0%B7%D0%B1%D0%B5%D0%BA%D0%B8%D1%81%D1%82%D0%B0%D0%BD/@41.2827379,69.1145567,11z/data=!3m1!4b1!4m6!3m5!1s0x38ae8b0cc379e9c3:0xa5a9323b4aa5cb98!8m2!3d41.2994958!4d69.2400734!16zL20vMGZzbXk?entry=ttu" target="_blank_">Время работы: с 11:00 до 23:00</a>
            </div>
            <div className="account">
              <img src={Account} alt="" />
              <span>Войти в аккаунт</span> 
            </div>



          </div>
        </div>
      </header>
      
        <nav>
          <div className="nav container">
            <NavLink to={"/"} className="logo">
              <a href="/"><img src={navLogo} alt="" />
              <span>Куда Пицца</span>
              </a>
            </NavLink>
            
              <NavLink to="/cart" className="shop">
                  <img src={Shopping} alt="" />
                  <span>({cartProducts.length}) $</span>
              </NavLink>
          </div>
        </nav>
    </div>
  )
}

export default Header