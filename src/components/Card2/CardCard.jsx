import React, { useContext } from 'react'
import PropTypes from "prop-types";
import { ProductContext } from '../../context/ProductContext';
import './Card.css';

const CardCard = ({id, filter, name, image, description, price, quantity}) => {
  const { increaseQuantity, decreaseQuantity } = useContext(ProductContext);
  return (
      <div className='flex_card container'>
          <div className='left'>
              <img src={image} alt="" />
              <div>
                <h3>{name}</h3><br />
                <p>{description}</p>
              </div>
          </div>
          <div className='right'>
            <div>
              <button onClick={() => decreaseQuantity(id)}>-</button>
              <button>{quantity}</button>
              <button onClick={() => increaseQuantity(id)}>+</button>
            </div>
            <div>
              <p className='price'>{price}  ₽</p>
            </div>
          </div>
      </div>
  )
}
CardCard.propTypes = {
  id: PropTypes.number,
  filter: PropTypes.string,
  image: PropTypes.string,
  name: PropTypes.string,
  description: PropTypes.string,
  price: PropTypes.string
};

export default CardCard