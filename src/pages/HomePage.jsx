import Showcase1 from "../components/showcase/Showcase"
import Location from "../assets/Location.png"
import Filter from "../assets/Filter.png"
import './homepage.css'
import { Col } from 'react-bootstrap'
import { products } from "../data/products"
import ProductCard from "../components/ProductCard/ProductCard"

import {
  BeveragesProducts,
  ComboProducts,
  SaucesProducts,
  DessertsProducts,
  pizzaProducts,
  sushiProducts,
} from "../data/categories";
import { useState } from "react"


const HomePage = () => {

  const [selectedFilters, setSelectedFilters] = useState({});

  const handleFilterChange = (category, event) => {
    setSelectedFilters((prevFilters) => ({
      ...prevFilters,
      [category]: event.target.value,
    }));
  };


  const filterProductsByFilter = (products, category, filter) => {
    if (filter === "") {
      return products;
    }
    return products.filter(
      (product) => product.category === category && product.filter === filter
    );
  };
  
  return (
    <div style={{background: "#f7f7f7", paddingBottom: "50px"}}>
      <div className="container">
        <Showcase1 />

        <div className="search">
          <div>
            <p>Проверить адрес доставки</p> 
          </div>
          <div className="inp-fl">
            <label htmlFor="in"><img style={{paddingTop: "3px"}} src={Location} alt="" /></label>
            <input type="text" id="in" placeholder="Адрес"/>
          </div>
            <button>Проверить</button>
        </div>
        
        {/* <div className="card">
          {products.map((pr) => (
            <Col key={pr.id}>
              <ProductCard {...pr} />
            </Col>
          ))}
        </div> */}

   
        <section>
          <div className="head_title">
            <h1>Пицца</h1>
              <div className="filter">
              <img src={Filter} alt="" />
              <select name="cars" id="cars">
                <option value="volvo">Фильтры</option>
                <option value="saab">Фильтры</option>
                <option value="mercedes">Фильтры</option>
                <option value="audi">Фильтры</option>
              </select>
              </div>
          </div>
          <div className="card">
            {filterProductsByFilter(
              pizzaProducts,
              "Pizza",
              selectedFilters["Pizza"] || ""
            ).map((pr) => (
              <ProductCard key={pr.id} {...pr} />
            ))}
          </div>
        </section>
        
        <section>
          <div className="head_title">
            <h1>Суши</h1>
              <div className="filter">
              <img src={Filter} alt="" />
              <select name="cars" id="cars">
                <option value="volvo">Фильтры</option>
                <option value="saab">Фильтры</option>
                <option value="mercedes">Фильтры</option>
                <option value="audi">Фильтры</option>
              </select>
              </div>
          </div>
          <div className="card">
            {filterProductsByFilter(
              sushiProducts,"Sushi",
              selectedFilters["Sushi"] || "").map((pr) => (
              <ProductCard key={pr.id} {...pr} />
            ))}
          </div>
        </section>

       

        <section>
          <div className="head_title">
            <h1>Десерты</h1>
              <div className="filter">
              <img src={Filter} alt="" />
              <select name="cars" id="cars">
                <option value="volvo">Фильтры</option>
                <option value="saab">Фильтры</option>
                <option value="mercedes">Фильтры</option>
                <option value="audi">Фильтры</option>
              </select>
              </div>
          </div>
          <div className="card">
            {filterProductsByFilter(
              DessertsProducts,
              "Desserts",
              selectedFilters["Desserts"] || ""
            ).map((pr) => (
              <ProductCard key={pr.id} {...pr} />
            ))}
          </div>
        </section>

        <section>
          <div className="head_title">
            <h1>Десерты</h1>
              <div className="filter">
              <img src={Filter} alt="" />
              <select name="cars" id="cars">
                <option value="volvo">Фильтры</option>
                <option value="saab">Фильтры</option>
                <option value="mercedes">Фильтры</option>
                <option value="audi">Фильтры</option>
              </select>
              </div>
          </div>
          <div className="card">
            {filterProductsByFilter(
              DessertsProducts,
              "Desserts",
              selectedFilters["Desserts"] || ""
            ).map((pr) => (
              <ProductCard key={pr.id} {...pr} />
            ))}
          </div>
        </section>

        <section>
          <div className="head_title">
            <h1>Десерты</h1>
              <div className="filter">
              <img src={Filter} alt="" />
              <select name="cars" id="cars">
                <option value="volvo">Фильтры</option>
                <option value="saab">Фильтры</option>
                <option value="mercedes">Фильтры</option>
                <option value="audi">Фильтры</option>
              </select>
              </div>
          </div>
          <div className="card">
            {filterProductsByFilter(
              DessertsProducts,
              "Desserts",
              selectedFilters["Desserts"] || ""
            ).map((pr) => (
              <ProductCard key={pr.id} {...pr} />
            ))}
          </div>
        </section>

        <section>
          <div className="head_title">
            <h1>Напитки</h1>
              <div className="filter">
              <img src={Filter} alt="" />
              <select name="cars" id="cars">
                <option value="volvo">Фильтры</option>
                <option value="saab">Фильтры</option>
                <option value="mercedes">Фильтры</option>
                <option value="audi">Фильтры</option>
              </select>
              </div>
          </div>
          <div className="card">
            {filterProductsByFilter(
              BeveragesProducts,
              "Beverages",
              selectedFilters["Beverages"] || ""
            ).map((pr) => (
              <ProductCard key={pr.id} {...pr} />
            ))}
          </div>
        </section>

        <section>
          <div className="head_title">
            <h1>Соусы</h1>
              <div className="filter">
              <img src={Filter} alt="" />
              <select name="cars" id="cars">
                <option value="volvo">Фильтры</option>
                <option value="saab">Фильтры</option>
                <option value="mercedes">Фильтры</option>
                <option value="audi">Фильтры</option>
              </select>
              </div>
          </div>
          <div className="card">
            {filterProductsByFilter(
              SaucesProducts,
              "Sauces",
              selectedFilters["Sauces"] || ""
            ).map((pr) => (
              <ProductCard key={pr.id} {...pr} />
            ))}
          </div>
        </section>

        <section>
          <div className="head_title">
            <h1>Комбо</h1>
              <div className="filter">
              <img src={Filter} alt="" />
              <select name="cars" id="cars">
                <option value="volvo">Фильтры</option>
                <option value="saab">Фильтры</option>
                <option value="mercedes">Фильтры</option>
                <option value="audi">Фильтры</option>
              </select>
              </div>
          </div>
          <div className="card">
            {filterProductsByFilter(
              ComboProducts,
              "Combo",
              selectedFilters["Combo"] || ""
            ).map((pr) => (
              <ProductCard key={pr.id} {...pr} />
            ))}
          </div>
        </section>

        <section className="txt_combo_tagi">
          <h1>Доставка пиццы в Москве</h1><br />
          <p>Захотелось чего-то вкусного и сытного? Желание простое и понятное, только в холодильнике все не то, и до магазина идти лень. Все пропало? Нет. Недорого заказать пиццу в Москве очень просто! Вам на помощь спешит супергерой – Domino’s Pizza! Как у всякого супергероя, у Domino’s Pizza есть свои суперсилы: восхитительный вкус продукции из отборных ингредиентов; широкий ассортимент, включающий легендарные, фирменные и классические виды, для вегетарианцев и любителей экспериментировать; быстрая и бесплатная доставка пиццы в течение 30 минут, чтобы вкусное и ароматное блюдо не успевало остыть.
            <br /><br />
            <b>Как сделать заказ</b> <br />
            Доставка пиццы от Domino’s – это когда Вам не нужно никуда ехать или звонить, ведь есть Интернет. Никогда еще заказ пиццы на дом в Москве не был таким простым! Чтобы заказать пиццу онлайн, Вам необходимо: выбрать понравившийся вариант и количество порций; положить желаемое в «Корзину»; не уходить далеко, так как вкусная пицца на заказ с доставкой уже мчится к Вам из ближайшей пиццерии Domino’s. И не забудьте оплатить заказ курьеру!</p><br />
            <a href="/">Показать полностью</a>
        </section>
      </div>
    </div>
  )
}

export default HomePage